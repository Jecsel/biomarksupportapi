class CreateProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :profiles do |t|
      t.belongs_to        :user
      t.string            :last_name
      t.string            :first_name
      t.string            :middle_name
      t.string            :ic
      t.string            :gender
      t.string            :birthday
      t.string            :phone_number
      t.string            :employer
      t.string            :nationality
      t.string            :email
      t.timestamps
    end
  end
end
