ActiveRecord::Schema.define(version: 2019_10_08_144007) do

  create_table "profiles", force: :cascade do |t|
    t.integer "user_id"
    t.string "last_name"
    t.string "first_name"
    t.string "middle_name"
    t.string "ic"
    t.string "gender"
    t.string "birthday"
    t.string "phone_number"
    t.string "employer"
    t.string "nationality"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "password"
    t.string "private_key"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
