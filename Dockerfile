FROM ruby:2.5.3-alpine3.8

# Minimal requirements to run a Rails app
RUN apk add --no-cache --update build-base \
                                linux-headers \
                                git \
                                mysql-client \
                                mysql-dev  \
                                nodejs \
                                tzdata

ENV APP_PATH /rails-api

# Create a directory for our application
# and set it as the working directory
RUN mkdir $APP_PATH
WORKDIR $APP_PATH
# Add our Gemfile and install gems
ADD Gemfile $APP_PATH
ADD Gemfile.lock $APP_PATH
RUN gem install bundler
RUN bundle install

# Copy the application into the container
COPY . APP_PATH
EXPOSE 3000