class UserController < ApplicationController
    before_action :authorized_user, except:[:create, :login]
    
    def validate
        profile = Profile.find_by_user_id @current_user.id
        render json:{user_type:@current_user.user_type_id , has_profile:profile.present?} 
    end

    def index
        @user = User.all
        render json: @user
    end

    def create 
        user = User.find_by_username create_params[:username]
        
        if user.present?
            render json: {message:"Username already exist"},status:403 #FORBIDDEN
            return false
        end

        User.create create_params
        render json: :created
    end

    def login 
        user = User.find_by_username login_params[:username]
        if user.present?
            if user.valid_password? login_params[:password]
                #return token
                user.private_key = "TEST001" #sample only
                user.save!
                token = {
                    user_id: user.id,
                    prv_key: user.private_key
                }
                bearer_token = encode token

                    render json: {token: bearer_token}
            else
                invalid_account
            end
        else
            invalid_account
        end
    end

    private 
    def invalid_account
        render json: {message:"Invalid Account"},status:403 #forbidden
        return false
    end
    def login_params
        #allow only expected parameters
        params.require(:credential).permit(:username, :password)
    end
    def create_params 
        #allow only expected parameters
        params.require( :user ).permit( :username, :password)
    end
end
