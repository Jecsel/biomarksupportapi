class ApplicationController < ActionController::API
    SECRET_KEY = Rails.application.secrets.secret_key_base.to_s
    def authorized_user
        token = request.headers['x-session-token']
        p token
        if token == 0 || token == "" 
            render json: {message:"Unauthorized",code:403},status:403
        else
        data = decode(token)
        
        @current_user = User.find(data["user_id"])


        if @current_user.nil?
            render json: {message:"Unauthorized",code:403},status:403
        end
        if @current_user.private_key != data["prv_key"]
            render json: {message:"Unauthorized",code:403},status:403
        end
        return @current_user
        end
    end 
    def encode(payload, exp = 24.hours.from_now)
        payload[:exp] = exp.to_i
        JWT.encode(payload, SECRET_KEY)
    end
    
    def decode(token)
        decoded = JWT.decode(token, SECRET_KEY)[0]
        HashWithIndifferentAccess.new decoded
    end
end
