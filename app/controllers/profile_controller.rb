class ProfileController < ApplicationController
    before_action :authorized_user

    # def index
    #     if @current_user.profile.present?
    #         render json: @current_user
    #     else
    #         render json:{status:false}
    #     end
    # end

    # def show
    #     @profile = Profile.find_by_user_id params[:id]
    #     render json: {profile: @profile}
    # end

    def create 
        profile = @current_user.profile.create profile_params
        # profile = Profile.find_by_first_name profile_params[:first_name]
        if profile.present?
            render json: {message:"Username already exist"},status:403 #FORBIDDEN
            return false
        end
        Profile.create profile_params
        render json:{is_register:true}
    end

    private
    def profile_params 
        params.require(:profiles).permit(
            :last_name,
            :first_name,
            :middle_name,
            :ic,
            :gender,
            :birthday,
            :phone_number,
            :employer,
            :nationality,
            :email,
        )
    end

end
