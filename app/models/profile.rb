class Profile < ApplicationRecord
    def fullname
        "#{self.first_name} #{self.middle_name} #{self.last_name}"
    end

    def name_only
        "#{self.first_name} #{self.last_name}"
    end

    def short
		"#{self.first_name[0]}#{self.last_name[0]}".upcase
    end
    
    
end
