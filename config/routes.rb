Rails.application.routes.draw do

  resources :user, only:[:index, :create] do
    collection do
      post 'login'
      get 'profile'
    end
  end
  
  resources :profile, only:[:create] do
  end
end
